
;■条件
;・キャラクターはあかね、やまとを使用
;・やまとの表情はnormal、angry使用不可
;・背景は何を使用してもOK

;■スクリプトを組む上でのポイント
;・真剣な表情がない中でどう演出するか
;・後半の靴の話でどのような演出をするか

*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[position layer="message0" left=20 top=400 width=920 height=200 page=fore visible=true]

[position layer=message0 page=fore margint="45" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=24 x=50 y=410]

[chara_config ptext="chara_name_area"]

;------------------------------初期設定-----------------------------------------
;------------------------------キャラ設定---------------------------------------
;あかね定義
[chara_new name="akane" storage="chara/akane/normal.png" jname="あかね"]
;あかね表情差分
[chara_face name="akane" face="normal" storage="chara/akane/normal.png"]
[chara_face name="akane" face="normal2" storage="chara/akane/normal2.png"]
[chara_face name="akane" face="yorokobi" storage="chara/akane/happy.png"]
[chara_face name="akane" face="yorokobi2" storage="chara/akane/happy2.png"]
[chara_face name="akane" face="odoroki" storage="chara/akane/doki2.png"]
[chara_face name="akane" face="odoroki2" storage="chara/akane/doki2.png"]
[chara_face name="akane" face="kanasimi" storage="chara/akane/sad.png"]

;やまと定義
[chara_new name="yamato" storage="chara/yamato/normal.png" jname="やまと"]
;やまと表情差分
[chara_face name="yamato" face="yorokobi" storage="chara/yamato/happy.png"]
[chara_face name="yamato" face="yorokobi2" storage="chara/yamato/happy2.png"]
[chara_face name="yamato" face="back" storage="chara/yamato/back.png"]
[chara_face name="yamato" face="tohoho" storage="chara/yamato/tohoho.png"]

;-------------------------キーフレーム1定義------------------------------------
[keyframe name="syukusyouhyouzi"]
[frame p=100% scale=0.6]
[endkeyframe]
;-------------------------キーフレーム1定義終了--------------------------------
;-------------------------------ここから開始-----------------------------------

[mask]
;メニューボタン表示
@showmenubutton
;背景→廊下
[bg storage=rouka.jpg time=0]
;BGM再生
[playbgm storage="sample.ogg"]
[mask_off time=500]

[wait time=1000]

;画面中央にあかねを表示
[chara_show name="akane" left=260 top=50]
[wait time=1500]

;SE→扉を開ける音
[playse storage="class_door1.ogg" loop="false"]
[mask effect="slideInRight" time=700]
[chara_hide name="akane"]
;背景→教室
[bg storage=room.jpg time=0]
[mask_off effect="slideOutLeft" time=700]

;画面外にあかねを縮小表示
[chara_mod name="akane" face="normal2" time=0]
[chara_show name="akane" left=1000 top=10]
[kanim name="akane" keyframe="syukusyouhyouzi" time=0]
;画面左にやまとを表示
[chara_mod name="yamato" face="back" reflect="true"]
[chara_show name="yamato" left=30 top=-20]

;あかね右からフェードイン
[anim name="akane" time=100 left=570 effect="easeInOutSine" time=1500]
[wa]

[chara_mod name="akane" face="odoroki2"]

;あかね小ジャンプ
;SE→ジャンプ音
[playse storage="jump1.ogg" loop="false"]
[anim name="akane" top="-=10" effect="easeInOutSine" time=100]
[wa]

[anim name="akane" top="+=10" effect="easeInOutSine" time=100]

;BGM停止
[fadeoutbgm time=1000]
@layopt layer=message0 visible=true

#あかね
やまとがすごく真剣な顔をしてる……。[r][l]

[chara_hide_all time=400]
[wait time=500]
[chara_mod name="akane" face="odoroki2" time=0 opacity=0]
[chara_show name="akane" left=600 top=50 time=0 opacity=0]
[anim name="akane" time="0"  opacity=0]
[anim name="akane" time=100 left="-=120" effect="easeInOutSine" time=500 opacity=255]
;下記台詞は「何『が』あったの！？」の脱字かと思われますので、『が』を追加しました。
何があったの！？[l]

[cm]

[chara_mod name="yamato" face="tohoho"]
[chara_show name="yamato" left=80 top=-30]

;BGM再生
[playbgm storage="normal.ogg"]

#やまと
SNSにどの写真載せようか悩んでるんだよ……[l]

[cm]

[chara_mod name="akane" face="yorokobi"]

#あかね
楽しそうなの載せたら良いと思うな！[l]

[cm]

[chara_mod name="yamato" face="yorokobi"]

#やまと
じゃあ一緒に撮ろうぜ！[l]

[cm]

[chara_mod name="akane" face="kanasimi"]


#あかね
でも、ただ載せるんじゃ面白くないよ？[l]

[cm]

[chara_mod name="yamato" face="tohoho"]

#やまと
じゃあ……[l]

[cm]

[wait time=1000]

[chara_mod name="akane" face="yorokobi"]

;あかね小ジャンプ
;SE→ジャンプ音
[playse storage="jump1.ogg" loop="false"]
[anim name="akane" top="-=10" effect="easeInOutSine" time=100]
[wa]

[anim name="akane" top="+=10" effect="easeInOutSine" time=100]


#あかね
あ！[r]
やまと新しい靴履いてたよね！[r]
私も買ったばかりだから、靴履いて写真撮ってみない？[l]

[cm]

;暗転
[mask time=800]
[chara_hide_all time=0]
;画面中央にあかねとやまとを縮小表示
[chara_mod name="akane" face="normal2" time=0]
[chara_show name="akane" left=380 top=10 time=0]
[kanim name="akane" keyframe="syukusyouhyouzi" time=0]
[chara_mod name="yamato" face="yorokobi2" time=0]
[chara_show name="yamato" left=-20 top=-70 time=0]
[kanim name="yamato" keyframe="syukusyouhyouzi" time=0]
;暗転解除
[mask_off time=1800]

#やまと
じゃあ、撮るぜ！[l]

[cm]

;カメラズーム
[camera y=-190 zoom=2.5 time=500 ease_type=ease-in]

;あかね小ジャンプ
;SE→ジャンプ音
[playse storage="jump1.ogg" loop="false"]
[anim name="akane" top="-=10" effect="easeInOutSine" time=100]
[wa]

[anim name="akane" top="+=10" effect="easeInOutSine" time=100]
[wa]

;あかね小ジャンプ
;SE→ジャンプ音
[playse storage="jump1.ogg" loop="false"]
[anim name="akane" top="-=10" effect="easeInOutSine" time=100]
[wa]

[anim name="akane" top="+=10" effect="easeInOutSine" time=100]
[wa]

#あかね
ちょっと待て！[r]
やっぱり、もう少し靴を近くした方がよくない？[l]

[cm]

;あかねに寄るやまと
[anim name="yamato" time=800 left="+=40" effect="easeInOutSine"]

#やまと
こんな感じか？[p]

;カメラズームアウト
[camera y=-30 zoom=1.5 time=500 ease_type=ease-in]

#やまと
なら……はい、チーズ[l]

[cm]

;カメラフラッシュ風演出
;SE→フラッシュ音
[playse storage="slash.ogg" loop="false"]
[mask time=20 color=0xffffff]
[wait time=20]
[chara_mod name="akane" face="yorokobi2" time=0]
[mask_off time=20]

[wait time=1500]

;暗転
[mask time=800]
[reset_camera]
[chara_hide_all time=0]
;暗転解除
[mask_off time=1800]

[chara_mod name="akane" face="normal" time=0 opacity=0]
[chara_show name="akane" left=600 top=50 time=0 opacity=0]
[anim name="akane" time="0"  opacity=0]
[anim name="akane" time=100 left="-=120" effect="easeInOutSine" time=500 opacity=255]


#あかね
いい感じの写真撮れた？[l]

[cm]

[chara_mod name="yamato" face="yorokobi"]
[chara_show name="yamato" left=80 top=-30]

;やまと小ジャンプ
;SE→ジャンプ音
[playse storage="jump2.ogg" loop="false"]
[anim name="yamato" top="-=10" effect="easeInOutSine" time=100]
[wa]

[anim name="yamato" top="+=10" effect="easeInOutSine" time=100]

#やまと
これならSNSに載せられそうだな！[l]

[cm]

[chara_mod name="akane" face="yorokobi"]

@layopt layer=message0 visible=false

;BGM停止
[fadeoutbgm time=4000]
;暗転
[mask time=4000]
[chara_hide_all]
[freeimage layer="base"]
[mask_off]
[jump first.ks]
;-------------------------------ここで終了-------------------------------------