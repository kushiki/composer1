
;■条件
;・使用キャラクターは自由
;・背景は何を使用してもOK

;■スクリプトを組む上でのポイント
;・急に話が飛んでいる場合に、場所などをユーザーが理解できるようにスクリプトが組めるか
;・キャラ性を理解できているか

*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[position layer="message0" left=20 top=400 width=920 height=200 page=fore visible=true]

[position layer=message0 page=fore margint="45" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=24 x=50 y=410]

[chara_config ptext="chara_name_area"]

;------------------------------初期設定-----------------------------------------
;------------------------------キャラ設定---------------------------------------
;あかね定義
[chara_new name="akane" storage="chara/akane/normal.png" jname="あかね"]
;あかね表情差分
[chara_face name="akane" face="normal" storage="chara/akane/normal.png"]
[chara_face name="akane" face="happy" storage="chara/akane/happy.png"]
[chara_face name="akane" face="angry" storage="chara/akane/angry.png"]
[chara_face name="akane" face="back" storage="chara/akane/back.png"]

;やまと定義
[chara_new name="yamato" storage="chara/yamato/normal.png" jname="やまと"]
;やまと表情差分
[chara_face name="yamato" face="tohoho" storage="chara/yamato/tohoho.png"]
[chara_face name="yamato" face="angry" storage="chara/yamato/angry.png"]
[chara_face name="yamato" face="sad" storage="chara/yamato/sad.png"]

;モブ女子1定義
[chara_new name="mob1" storage="chara/hi/hi01.png"]
;モブ女子1表情差分
[chara_face name="mob1" face="odoroki" storage="chara/hi/hi04.png"]

;モブ女子2定義
[chara_new name="mob2" storage="chara/hito/hito01-1.png"]
;モブ女子2表情差分
[chara_face name="mob2" face="odoroki" storage="chara/hito/hito04.png"]

;-------------------------キーフレーム1定義------------------------------------
[keyframe name="sakamitidash"]
[frame p=10% y=-75]
[frame p=20% y=-150 scale=0.9]
[frame p=30% y=-225 scale=0.8]
[frame p=40% y=-300 scale=0.7]
[frame p=50% y=-375 scale=0.6]
[frame p=60% y=-450 scale=0.5]
[frame p=70% y=-525 scale=0.4]
[frame p=80% y=-600 scale=0.3]
[frame p=90% y=-675 scale=0.2]
[frame p=100% y=-750 scale=0.1]
[endkeyframe]
;-------------------------キーフレーム1定義終了--------------------------------
;-------------------------キーフレーム2定義------------------------------------
[keyframe name="roukadash"]
[frame p=10% y=-75]
[frame p=20% y=-150 scale=0.9]
[frame p=30% y=-225 scale=0.8]
[frame p=40% y=-300 scale=0.7]
[frame p=50% y=-375 scale=0.6]
[frame p=60% y=-450 scale=0.5]
[frame p=70% y=-500 scale=0.4]
[frame p=80% y=-550 scale=0.3]
[frame p=90% y=-600 scale=0.2]
[frame p=100% y=-650 scale=0.1]
[endkeyframe]
;-------------------------キーフレーム2定義終了--------------------------------
;-------------------------キーフレーム3定義------------------------------------
[keyframe name="syukusyouhyouzi"]
[frame p=100% scale=0.5]
[endkeyframe]
;-------------------------キーフレーム3定義終了--------------------------------
;-------------------------キーフレーム4定義------------------------------------
[keyframe name="buruburu"]
[frame p=10% x=-10]
[frame p=20% x=0]
[frame p=30% x=-10]
[frame p=40% x=0]
[frame p=50% x=-10]
[frame p=60% x=0]
[frame p=70% x=-10]
[frame p=80% x=0]
[frame p=90% x=-10]
[frame p=100% x=0]
[endkeyframe]
;-------------------------キーフレーム4定義終了--------------------------------

;-------------------------------ここから開始-----------------------------------
[mask]
[wait time=3000]
[mask_off]

;メニューボタン表示
@showmenubutton

;SE→すずめの鳴き声→フェードイン
[fadeinse storage=suzume.ogg loop=false time=5000]

;背景→青空
[bg storage=aosora.jpg time="4000"]
[wait time=4000]
;SE→すずめの鳴き声→フェードアウト
;一緒に暗転
[fadeoutse time=5000]
[mask effect="fadeInUpBig" time=3000]

;背景切り替え→坂道
[bg storage="sakamiti.jpg" time=0]
;画面外(下)にあかね(バック)を配置
[chara_show name="akane" left=360 top=650]
[chara_mod name="akane"  face="back"]

;BGM再生
[fadeinbgm storage="normal.ogg" loop="false" time=500]
[mask_off effect="fadeOutUpBig" time=3000]

;坂道ダッシュするあかね
[kanim name="akane" keyframe="sakamitidash" time=1200]
;SE→走り去る音
[playse storage="landing.ogg" loop="false" buf=0]
[wait time=150]

[playse storage="landing.ogg" loop="false"buf=1]
[wait time=150]

[playse storage="landing.ogg" loop="false"buf=2]
[wait time=150]

[playse storage="landing.ogg" loop="false"buf=3]
[wait time=150]

[playse storage="landing.ogg" loop="false"buf=4]
[wait time=150]

[playse storage="landing.ogg" loop="false"buf=5]
[wait time=150]

[playse storage="landing.ogg" loop="false"buf=6]
[wait time=150]

[playse storage="landing.ogg" loop="false"buf=7]

;SE→走り去る音→フェードアウト
[fadeoutse time=300]
;暗転
[mask effect="fadeInUpBig" time=1500]
[chara_hide name="akane" time=0]

;背景切り替え→登校中の道
[bg storage="nc71313.jpg" time=0]
;画面外(右)にあかねを配置
[chara_show name="akane" left=1000 top=50]
[chara_mod name="akane" face="normal"]
;画面外(右)にやまとを配置
[chara_show name="yamato" left=1000 top=50]
[chara_mod name="yamato" face="angry"]

[mask_off effect="fadeOutLeftBig" time=2000]

;あかねが右からフェードイン
[anim name="akane" time=700 left=120 effect="easeInOutSine"]
[wa]

[anim name="akane" time=200 left=130 effect="easeInOutSine"]
[wait time=1000]

[chara_mod name="akane" face="normal" reflect="true"]
@layopt layer=message0 visible=true

#あかね
[font size=30]やまと！[r]
早く早くー！[p]

[chara_mod name="akane" face="angry" cross="true"]

#あかね
そんなのんびりしてると、遅刻しちゃうぞ！[l]
[resetfont]
[cm]

@layopt layer=message0 visible=false

[chara_mod name="akane" face="normal" reflect="false"]

[wait time=500]

;画面外へあかねがフェードアウト
[anim name="akane" time=1000 left=200 effect="easeInOutSine"]
[wa]

[anim name="akane" time=500 left="-=700" effect="easeInOutSine"]
;BGM停止
[fadeoutbgm time=2000]
[wait time=2000]

[chara_hide name="akane"]

;BGM再生
[fadeinbgm storage="comedy.ogg" loop="false" time=500]
;やまとが画面外から遅れて登場する
[anim name="yamato" time=6000 left=280 effect="easeInOutSine"]
[wa]

[wait time=1000]

@layopt layer=message0 visible=true

;上下にピョン
[anim name="yamato" time=100 top="-=100" effect="easeInOutSine"]
[wa]

[anim name="yamato" time=100 top="+=100" effect="easeInOutSine"]

#やまと
……分かってるって！[r][l]

[chara_mod name="yamato" face="tohoho" cross="true"]

この坂道きついんだよ！[l]

[cm]

@layopt layer=message0 visible=false

[anim name="yamato" time=6000 left="-=1000" effect="easeInOutSine"]

;BGM停止
[fadeoutbgm time=4000]
;暗転
[mask time=4000]
[chara_hide name="yamato"]

;場転用アイキャッチ演出
[bg storage="eyecatch.jpg" time=0]

[mask_off]
;アイキャッチBGM再生
[playbgm storage="sample.ogg"]

[wait time=5000]

;BGM停止
[fadeoutbgm time=4000]
;暗転
[mask time=4000]


;背景切り替え
[bg storage="gaikan_hiru.jpg" time=0]
;画面外(右)にあかねを配置
[chara_show name="akane" left=1000 top=50]
[chara_mod name="akane" face="normal"]

[mask_off time=2000 effect="fadeOutRightBig"]
;BGM再生
[fadeinbgm storage="normal.ogg" loop="false" time=500]

;画面を横切るあかね
[anim name="akane" time=1500 left="-=1500" effect="easeInOutSine"]
[wait time=400]

;SE→走る音
[playse storage="landing.ogg" loop="false" buf=0]
[wait time=150]

[playse storage="landing.ogg" loop="false"buf=1]
[wait time=150]

[playse storage="landing.ogg" loop="false"buf=2]
[wait time=150]

[playse storage="landing.ogg" loop="false"buf=3]
[wait time=150]

[playse storage="landing.ogg" loop="false"buf=4]

;暗転
[mask time=2000]
[chara_hide name="akane"]

;背景切り替え
[bg storage="rouka.jpg" time=0]
;画面外(下)にあかねを配置
[chara_show name="akane" left=150 top=650]
[chara_mod name="akane" face="back" reflect="true"]

[mask_off time=1200 effect="fadeOutRightBig"]

;廊下を走るあかね
[kanim name="akane" keyframe="roukadash" time=1200]

;SE→廊下を走る音
[playse storage="landing.ogg" loop="false" buf=0]
[wait time=150]

[playse storage="landing.ogg" loop="false"buf=1]
[wait time=150]

[playse storage="landing.ogg" loop="false"buf=2]
[wait time=150]

[playse storage="landing.ogg" loop="false"buf=3]
[wait time=150]

[playse storage="landing.ogg" loop="false"buf=4]
[wait time=150]

[playse storage="landing.ogg" loop="false"buf=5]
[wait time=150]

[playse storage="landing.ogg" loop="false"buf=6]
[wait time=150]

[playse storage="landing.ogg" loop="false"buf=7]

;暗転
[mask effect="fadeInUpBig" time=1500]
[chara_hide name="akane"]

;背景切り替え
[bg storage="room.jpg" time=0]
;画面外(右)にあかねを配置
[chara_show name="akane" left=1000 top=50]
[chara_mod name="akane" face="normal" reflect="true"]
;画面左奥にモブ女子1を縮小して配置
[chara_show name="mob1" left=100 top=0]
[kanim name="mob1" keyframe="syukusyouhyouzi" time=0]
;画面左奥にモブ女子2を縮小して配置
[chara_show name="mob2" left=-100 top=0]
[kanim name="mob2" keyframe="syukusyouhyouzi" time=0]

[mask_off time=2000 effect="fadeOutRightBig"]


;テキスト効果を追加
[mtext text="ガヤガヤ" x=200 y=100 size=30 in_effect="fadeIn" time=2 out_effect="fadeOut"]
[mtext text="ガヤガヤ" x=660 y=150 size=30 in_effect="fadeIn" time=2 out_effect="fadeOut"]
[wait time=1000]

;SE→勢いよく教室のドアが開く音
[playse storage="class_door2.ogg" loop="false"]
[wait time=1000]

;教室に勢いよく入ってくるあかね
[anim name="akane" time=800 left=350 effect="easeInOutSine"]
[wa]

;あかねの突然の登場に驚くモブたち
[anim name="akane" time=500 left=360 effect="easeInOutSine"]
[chara_mod name="mob1" face="odoroki"]
[chara_mod name="mob2" face="odoroki"]
[anim name="mob1" time=200 top=-50 effect="easeInOutSine"]
[anim name="mob2" time=200 top=-50 effect="easeInOutSine"]

[anim name="mob1" time=200 top=0 effect="easeInOutSine"]
[anim name="mob2" time=200 top=0 effect="easeInOutSine"]

@layopt layer=message0 visible=true

#あかね
到着！[p]

[chara_mod name="akane" face="happy" cross="true"]

#あかね
[font size=30]みんな、

;元気いっぱいのあかね演出
[anim name="akane" time=200 top=0 effect="easeInOutSine"]
[wa]

[anim name="akane" time=200 top=50 effect="easeInOutSine"]

おっはよー！[l]
[resetfont]
[cm]

@layopt layer=message0 visible=false
;BGM停止
[fadeoutbgm time=1000]
[chara_hide_all time=300]
[wait time=300]

;画面外(右)にやまとを表示
[chara_show name="yamato" left=1000 top=50]
[chara_mod name="yamato" face="sad"]
;画面外(左)にあかねを表示
[chara_show name="akane" left=-350 top=50]
[chara_mod name="akane" face="happy" reflect="false"]

;SE→ゆっくりドアが開く音
[playse storage="class_door1.ogg" loop="false"]
[wait time=1000]

;BGM再生
[playbgm storage="dozikkomarch.ogg"]
;よろよろ歩くやまと→一歩
[anim name="yamato" time=3000 left=800 effect="easeInOutSine"]
[wa]

[anim name="yamato" time=400 top=100 effect="easeInOutSine"]
[wait time=1500]


;よろよろ歩くやまと→二歩
[anim name="yamato" time=1000 top=0 effect="easeInOutSine"]
[wa]

[anim name="yamato" time=3000 left=640 effect="easeInOutSine"]
[wa]

[anim name="yamato" time=400 top=50 effect="easeInOutSine"]
[wait time=1500]


;よろよろ歩くやまと→三歩
[anim name="yamato" time=1000 top=0 effect="easeInOutSine"]
[wa]

[anim name="yamato" time=3000 left=470 effect="easeInOutSine"]
[wa]

[anim name="yamato" time=400 top=50 effect="easeInOutSine"]
[wait time=1500]

;SE→ギャグSE
[playse storage="チーンと意気消沈.ogg" loop="false"]
[wait time=2000]


@layopt layer=message0 visible=true

#やまと
[delay speed=150]はぁはぁ……[p]

#やまと
置いてくんじゃねえよ……

[chara_mod name="yamato" face="angry"]

;置いていったあかねに抗議をするやまと
[anim name="yamato" time=300 top=0]
[wa]

[kanim name="yamato" keyframe="buruburu" time=500]
[wait time=1000]
[resetdelay]
この体力馬鹿が！[l]

[cm]

[anim name="akane" time=2000 left=100 effect="easeInOutSine"]
[wa]

#あかね
ほら！[r]
授業始まっちゃうよー[p]

[chara_mod name="akane" face="normal" reflect="true"]
[wait time=300]

[anim name="akane" left=-380 effect="easeInOutSine" time=2000]

#あかね
席についてー[l]

[cm]

[chara_mod name="yamato" face="tohoho"]

#やまと
[delay speed=150]はあ……、[l]
[resetdelay]
わかったよ[l]

[cm]

@layopt layer=message0 visible=false

;BGM停止
[fadeoutbgm time=4000]
;暗転
[mask time=4000]
[chara_hide_all]
[freeimage layer="base"]
[mask_off]
[jump first.ks]
;-------------------------------ここで終了-------------------------------------