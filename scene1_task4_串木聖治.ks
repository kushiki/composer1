
;■条件
;・キャラクターはあかね、やまとを使用
;・背景は何を使用してもOK

;■スクリプトを組む上でのポイント
;・鏡を見ている際にどのような演出をするか

*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[position layer="message0" left=20 top=400 width=920 height=200 page=fore visible=true]

[position layer=message0 page=fore margint="45" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=24 x=50 y=410]

[chara_config ptext="chara_name_area"]

;------------------------------初期設定-----------------------------------------
;------------------------------キャラ設定---------------------------------------
;あかね定義
[chara_new name="akane" storage="chara/akane/normal.png" jname="あかね"]
;あかね表情差分
[chara_face name="akane" face="normal" storage="chara/akane/normal.png"]
[chara_face name="akane" face="odoroki" storage="chara/akane/doki.png"]
[chara_face name="akane" face="yorokobi" storage="chara/akane/happy.png"]
[chara_face name="akane" face="back" storage="chara/akane/back.png"]

;やまと定義
[chara_new name="yamato" storage="chara/yamato/normal.png" jname="やまと"]
;やまと表情差分
[chara_face name="yamato" face="normal" storage="chara/yamato/normal.png"]
[chara_face name="yamato" face="yorokobi" storage="chara/yamato/happy.png"]
[chara_face name="yamato" face="tohoho" storage="chara/yamato/tohoho.png"]

;鏡用あかね定義
[chara_new name="akane2" storage="chara/akane/normal2.png"]

;-------------------------キーフレーム1定義------------------------------------
[keyframe name="syukusyouhyouzi"]
[frame p=100% scale=0.6]
[endkeyframe]
;-------------------------キーフレーム1定義終了--------------------------------
;-------------------------------ここから開始-----------------------------------

[mask]
;メニューボタン表示
@showmenubutton
;背景→部室
[bg storage=gym.jpg time=0]
;SEループ→時計
[playse storage="clock.ogg" loop="true"]
;画面外に鏡用あかねを縮小表示
[chara_show name="akane2" left=1000 top=0 time=0 opacity=0]
[kanim name="akane2" keyframe="syukusyouhyouzi" time=0]
[mask_off time=3000]

;画面中央にあかねを表示
[chara_show name="akane" left=260 top=50]

[wait time=2000]
[fadeoutse]

@layopt layer=message0 visible=true
[bg storage=kuro.jpg method="fadeIn" time=2000]
;BGMをフェードイン再生
[fadeinbgm storage="sad.ogg" loop="false" time=2000]

#あかね
[delay speed=200]鏡よ、鏡よ、鏡さん[l]

[cm]

;鏡に向かい合っています演出
[chara_hide name="akane" time=600]
[chara_mod name="akane" face="back" time=0 opacity=0]
[chara_show name="akane" left=0 top=50 time=0 opacity=0]
[anim name="akane" time="0" opacity=0]
[anim name="akane" left="+=100" effect="easeInOutSine" time=3000 opacity=255]

[chara_move name="akane2" left=580 top=0 time=0 opacity=0]
[anim name="akane2" time="0" opacity=0]
[anim name="akane2" left="-=100" effect="easeInOutSine" time=3000 opacity=255]


#あかね
[delay speed=200]世界で一番美しいのはだーれ？[l]
[resetdelay]

[cm]

;BGMをフェードアウト停止
[fadeoutbgm time=3000]
[mask time=3000]
[chara_hide_all time=0]
;背景→部室
[bg storage=gym.jpg time=0]
;画面左にあかねを表示
[chara_mod name="akane" face="normal"]
[chara_show name="akane" left=80 top=50]
[mask_off time=1800]

;画面外（右）にやまとを表示
[chara_mod name="yamato" face="tohoho"]
[chara_show name="yamato" left=950 top=-30]

[anim name="yamato" left="-=490" effect="easeInOutSine" time=2800]
[wa]

#やまと
……おまえ、いったい何やってるんだ？？[l]

[cm]

[chara_mod name="akane" face="odoroki"]

;あかね小ジャンプ
;SE→ジャンプ音
[playse storage="jump1.ogg" loop="false"]
[anim name="akane" top="-=10" effect="easeInOutSine" time=100]
[wa]

[anim name="akane" top="+=10" effect="easeInOutSine" time=100]

[playbgm storage="comedy.ogg" loop="smooth"]

#あかね
や、やまと！？[l]

[cm]

#やまと
あまりにも真剣にやってるから、ちょっと声かけ辛くてな……[l]

[cm]

[anim name="akane" left="-=50" effect="easeInOutSine" time=1500]

#あかね
あはは……[p]

;BGMフェードアウト停止
[fadeoutbgm time=700]
[chara_mod name="akane" face="normal"]
[chara_mod name="yamato" face="normal"]
;BGM再生
[playbgm storage="sample.ogg" loop="smooth"]

#あかね
実は、演劇の練習をしていたんだ！[p]

[chara_mod name="akane" face="yorokobi"]

;あかね小ジャンプ
;SE→ジャンプ音
[playse storage="jump1.ogg" loop="false"]
[anim name="akane" top="-=10" effect="easeInOutSine" time=100]
[wa]

[anim name="akane" top="+=10" effect="easeInOutSine" time=100]

;この場面はキャラ１とキャラ２のみしか登場していないので、「『キャラ３ちゃん』も練習手伝ってよ！」の『』部分を『やまと』に修正致しました。
#あかね
あ！[r]
やまとも練習手伝ってよ！[r]
これ台本ね！[l]

[cm]

;台本をやまとに渡すあかね
[anim name="akane" left="+=200" effect="easeInOutSine" time=600]
[playse storage="人にぶつかりました.ogg" loop="false"]
[chara_mod name="yamato" face="tohoho"]
[anim name="yamato" left="+=5" time=50]
[wait time=300]

[anim name="akane" left="-=200" effect="easeInOutSine" time=600]
[wa]

#やまと
まだやるって言ってないんだが……[l]

[cm]

[chara_mod name="akane" face="normal"]

#あかね
まあまあ、そんなこと言わずに……[p]

;あかね小ジャンプ
;SE→ジャンプ音
[playse storage="jump1.ogg" loop="false"]
[anim name="akane" top="-=10" effect="easeInOutSine" time=100]
[wa]

[anim name="akane" top="+=10" effect="easeInOutSine" time=100]

#あかね
あとでアイス御馳走するから！[l]

[cm]

[chara_mod name="yamato" face="yorokobi"]

#やまと
やらせていただきます！[l]

[cm]

[chara_mod name="akane" face="yorokobi"]


#あかね
本当に！[r]
やった！[p]

;あかね小ジャンプ
;SE→ジャンプ音
[playse storage="jump1.ogg" loop="false"]
[anim name="akane" top="-=10" effect="easeInOutSine" time=100]
[wa]

[anim name="akane" top="+=10" effect="easeInOutSine" time=100]

;あかね小ジャンプ
;SE→ジャンプ音
[playse storage="jump1.ogg" loop="false"]
[anim name="akane" top="-=10" effect="easeInOutSine" time=100]
[wa]

[anim name="akane" top="+=10" effect="easeInOutSine" time=100]


#あかね
よーし！[r]
練習再開だ！[l]

[cm]


@layopt layer=message0 visible=false

;BGM停止
[fadeoutbgm time=4000]
;暗転
[mask time=4000]
[chara_hide_all]
[freeimage layer="base"]
[mask_off]
[jump first.ks]
;-------------------------------ここで終了-------------------------------------