
;■条件
;・使用キャラクターは自由
;・背景は何を使用してもOK

;■スクリプトを組む上でのポイント
;・抱き着く際の演出に違和感がないように
;・普通に組むと盛り上がりに欠けるので、どこかしらに演出を

*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[position layer="message0" left=20 top=400 width=920 height=200 page=fore visible=true]

[position layer=message0 page=fore margint="45" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=24 x=50 y=410]

[chara_config ptext="chara_name_area"]

;------------------------------初期設定-----------------------------------------;------------------------------キャラ設定---------------------------------------
;あかね定義
[chara_new name="akane" storage="chara/akane/normal.png" jname="あかね"]
;あかね表情差分
[chara_face name="akane" face="normal" storage="chara/akane/normal.png"]
[chara_face name="akane" face="normal2" storage="chara/akane/normal2.png"]
[chara_face name="akane" face="yorokobi" storage="chara/akane/happy.png"]
[chara_face name="akane" face="yorokobi2" storage="chara/akane/happy2.png"]
[chara_face name="akane" face="odoroki" storage="chara/akane/doki.png"]
[chara_face name="akane" face="kanasimi" storage="chara/akane/sad.png"]

;ひとみ定義
[chara_new name="hitomi" storage="chara/hi/hi01.png" jname="ひとみ"]
;ひとみ表情差分
[chara_face name="hitomi" face="normal" storage="chara/hi/hi01.png"]
[chara_face name="hitomi" face="yorokobi" storage="chara/hi/hi03.png"]
[chara_face name="hitomi" face="odoroki" storage="chara/hi/hi04.png"]
[chara_face name="hitomi" face="mumumu" storage="chara/hi/hi05.png"]
[chara_face name="hitomi" face="ikari1" storage="chara/hi/hi06.png"]
[chara_face name="hitomi" face="ikari2" storage="chara/hi/hi07.png"]

;-------------------------キーフレーム1定義------------------------------------
[keyframe name="yori"]
[frame p=10% scale=0.15]
[frame p=20% scale=0.2]
[frame p=30% scale=0.3]
[frame p=40% scale=0.4]
[frame p=50% scale=0.5]
[frame p=60% scale=0.6]
[frame p=70% scale=0.7]
[frame p=80% scale=0.8]
[frame p=90% scale=0.9]
[frame p=100% scale=1]
[endkeyframe]
;-------------------------キーフレーム1定義終了--------------------------------
;-------------------------キーフレーム2定義------------------------------------
[keyframe name="syukusyouhyouzi"]
[frame p=100% scale=0.1]
[endkeyframe]
;-------------------------キーフレーム2定義終了--------------------------------
;-------------------------キーフレーム3定義------------------------------------
[keyframe name="buruburu"]
[frame p=10% x=-10]
[frame p=20% x=0]
[frame p=30% x=-10]
[frame p=40% x=0]
[frame p=50% x=-10]
[frame p=60% x=0]
[frame p=70% x=-10]
[frame p=80% x=0]
[frame p=90% x=-10]
[frame p=100% x=0]
[endkeyframe]
;-------------------------キーフレーム3定義終了--------------------------------
;-------------------------------ここから開始-----------------------------------

[mask]
;メニューボタン表示
@showmenubutton
;背景→学校(夕方)
[bg storage=gaikan_yu.jpg time=0]
;画面外にあかねを縮小表示
[chara_show name="akane" left=1000 top=0]
[chara_mod name="akane" face="normal2" time=0]
[kanim name="akane" keyframe="syukusyouhyouzi" time=0]
;BGM再生
[playbgm storage="normal.ogg"]
;暗転解除
[mask_off time=2000]

[wait time=1000]

;画面左側にひとみ登場
[chara_show name="hitomi" left=100 top=50]
;SE→すずめの鳴き声
[playse storage="suzume.ogg" loop="true"]

;しばし鳥を眺めてるひとみ
[wait time=2000]
;鳥を見て顔がほころぶひとみ
[chara_mod name="hitomi" face="yorokobi"]
[wait time=3000]

;背景の通路みたいなところにあかねを表示
[chara_move name="akane" left=400 top=-20]

@layopt layer=message0 visible=true
;SE→すずめの鳴き声→フェードアウト
;BGMもフェードアウト
[fadeoutse time=1000]
[fadeoutbgm time=1000]
[wait time=1000]
[stopbgm]

#あかね
[font size=60]ひとみちゃーん！[l]
[resetfont]
[cm]

[chara_mod name="hitomi" face="mumumu"]

[anim name="akane" time=100 top=-40 effect="easeInOutSine"]
[playse storage="jump2.ogg"]
[wa]
[anim name="akane" time=100 top=-20 effect="easeInOutSine"]
[wa]
[anim name="akane" time=100 top=-40 effect="easeInOutSine"]
[playse storage="jump2.ogg"]
[wa]
[anim name="akane" time=100 top=-20 effect="easeInOutSine"]

[wait time=2000]

[chara_mod name="hitomi" face="mumumu" reflect="true"]
[wait time=1000]

[chara_mod name="hitomi" face="odoroki" time=100]
[wait time=100]

;あかねにカメラズーム
[camera x=120 y=10 zoom=3 time=500 ease_type=ease-in]
[wait_camera]

[chara_mod name="akane" face="yorokobi2" time=100]

;SE→ギャグSE
[playse storage="デデーン.ogg"]
[wait time=4000]

;上下にピョン×２
[anim name="akane" time=100 top=-40 effect="easeInOutSine"]
[playse storage="jump2.ogg"]
[wa]
[anim name="akane" time=100 top=-20 effect="easeInOutSine"]
[wa]
[anim name="akane" time=100 top=-40 effect="easeInOutSine"]
[playse storage="jump2.ogg"]
[wa]
[anim name="akane" time=100 top=-20 effect="easeInOutSine"]

#ひとみ
あかね！？[p]

;こっちへ走ってくるあかね
[kanim name="akane" keyframe="yori" time=1200]

[playse storage="landing.ogg" loop="false" buf=0]
[wait time=150]

[playse storage="landing.ogg" loop="false"buf=1]
[wait time=150]

[playse storage="landing.ogg" loop="false"buf=2]
[wait time=150]

[playse storage="landing.ogg" loop="false"buf=3]
[wait time=150]

[playse storage="landing.ogg" loop="false"buf=4]
[wait time=150]

[playse storage="landing.ogg" loop="false"buf=5]
[wait time=150]

[playse storage="landing.ogg" loop="false"buf=6]
[wait time=150]

[playse storage="landing.ogg" loop="false"buf=7]

;暗転
[mask time=500]
[chara_hide_all]
[reset_camera time=0]
;画面外(右)にあかねを配置
[chara_show name="akane" left=1000 top=50 time=0]
[chara_mod name="akane" face="yorokobi" time=0]
;画面中央にひとみを配置
[chara_show name="hitomi" left=280 top=50 time=0]
[chara_mod name="hitomi" face="mumumu" time=0]
;BGM再生
[playbgm storage="dozikkomarch.ogg" loop=smooth]
[mask_off effect="slideOutRight" time=500]


;ひとみに走って抱き着いてくるあかね
[anim name="akane" time=800 left=400 effect="easeInOutSine"]
[wait time=700]
[playse storage="jump1.ogg"]
[anim name="hitomi" time=200 left=240 effect="easeInOutSine"]
[wa]

[chara_mod name="hitomi" face="ikari2" time=100]
[wait time=1000]

#ひとみ
急に抱き着かないでよ！[l]

[chara_mod name="hitomi" face="ikari1"]

[cm]

[chara_mod name="akane" face="kanasimi"]

#あかね
えー！[r]
いいじゃん[p]

[chara_mod name="akane" face="normal"]

#あかね
ところで、ひとみちゃんは何やってるの？[l]

[cm]

[chara_mod name="hitomi" face="mumumu"]

#ひとみ
[delay speed=150]はぁ……、[l]
[resetdelay]

[chara_mod name="hitomi" face="mumumu" reflect="true"]

珍しい鳥がいたから見てたのよ[l]

[cm]

[chara_mod name="akane" face="odoroki"]

#あかね
[font size=40]え！[r][l]

;キョロキョロするあかね
[chara_mod name="akane" face="odoroki" reflect="true"]
[anim name="akane" time=500 left=500 effect="easeInOutSine"]
[wa]

[chara_mod name="akane" face="odoroki" reflect="false"]
[anim name="akane" time=500 left=400 effect="easeInOutSine"]
[wa]

[chara_mod name="akane" face="odoroki" reflect="true"]
[anim name="akane" time=500 left=500 effect="easeInOutSine"]
[wa]

どこどこ！？[p]
[resetfont]

[wait time=1000]

[chara_mod name="akane" face="normal" time=100 reflect="true"]

[anim name="akane" time=100 top=0 effect="easeInOutSine"]
[playse storage="jump2.ogg"]
[wa]
[anim name="akane" time=100 top=50 effect="easeInOutSine"]

#あかね
あ！[r][l]

[chara_mod name="hitomi" face="normal" reflect="false"]

[chara_mod name="akane" face="normal" reflect="false"]
[anim name="akane" time=500 left=430 effect="easeInOutSine"]

そういえば、これから新しく出来たカフェ行くんだけど、一緒に行かない？[l]

[cm]

[chara_mod name="hitomi" face="ikari1" reflect="true"]

#ひとみ
もう少し鳥を見たいから遠慮しとくわ[l]

[cm]

[chara_mod name="akane" face="odoroki"]
[anim name="akane" time=250 left=500 effect="easeInOutSine"]
[wa]
[playse storage="が～ん.ogg"]
[kanim name="akane" keyframe="buruburu" time=500]
[wait time=4000]

#あかね
えー！[r]

[chara_mod name="akane" face="kanasimi"]
[anim name="akane" time=500 left=400 effect="easeInOutSine"]

行こうよー[l]

[cm]

;抱き着いてひとみを揺するあかね
[anim name="akane" time=500 left=200 effect="easeInOutSine"]
[anim name="hitomi" time=500 left=40 effect="easeInOutSine"]
[wa]

[anim name="akane" time=500 left=600 effect="easeInOutSine"]
[anim name="hitomi" time=500 left=440 effect="easeInOutSine"]
[wa]

[anim name="akane" time=500 left=200 effect="easeInOutSine"]
[anim name="hitomi" time=500 left=40 effect="easeInOutSine"]
[wa]

[anim name="akane" time=500 left=400 effect="easeInOutSine"]
[anim name="hitomi" time=500 left=240 effect="easeInOutSine"]
[wa]

[chara_mod name="hitomi" face="ikari1" reflect="false"]

#ひとみ
ちょっと人前で恥ずかしいから辞めなさい！[p]

#ひとみ
[delay speed=150]と言うより……[l]
[resetdelay]

[chara_mod name="hitomi" face="ikari2"]
[camera x=40 y=60 zoom=1.6 time=500]

[font size=40]いつまで抱き着いてるのよ！[l]

[cm]

@layopt layer=message0 visible=false

;BGM停止
[fadeoutbgm time=4000]
;暗転
[mask time=4000]
[reset_camera time=0]
[chara_hide_all]
[freeimage layer="base"]
[mask_off]
[jump first.ks]
;-------------------------------ここで終了-------------------------------------