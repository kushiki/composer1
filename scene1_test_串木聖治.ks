*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[mask time=0]

[position layer="message0" left=20 top=450 width=920 height=150 page=fore visible=true]

[position layer=message0 page=fore margint="45" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=22 x=50 y=460]
[chara_config ptext="chara_name_area"]

;------------------------------初期設定----------------------------------------
;------------------------------キャラ設定--------------------------------------
;あかね定義
[chara_new name="akane" storage="chara/akane/normal.png" jname="あかね"]
[chara_new name="akane2" storage="chara/akane/normal.png" jname="あかね"]
[chara_new name="akane3" storage="chara/akane/normal2.png" jname="あかね"]

;あかね表情差分
[chara_face name="akane" face="normal" storage="chara/akane/normal.png"]
[chara_face name="akane" face="yorokobi" storage="chara/akane/happy.png"]
[chara_face name="akane" face="ikari" storage="chara/akane/angry.png"]
[chara_face name="akane" face="kanasimi" storage="chara/akane/sad.png"]
[chara_face name="akane" face="odoroki" storage="chara/akane/doki.png"]
[chara_face name="akane3" face="yorokobi" storage="chara/akane/happy2.png"]

;ひぃ定義
[chara_new name="hi" storage="chara/hi/hi01.png" jname="ひぃ"]
[chara_new name="hi2" storage="chara/hi/hi01.png" jname="ひぃ"]

;ひぃ表情差分
[chara_face name="hi" face="normal" storage="chara/hi/hi01.png"]
[chara_face name="hi" face="mumu" storage="chara/hi/hi02.png"]
[chara_face name="hi" face="yorokobi" storage="chara/hi/hi03.png"]
[chara_face name="hi" face="odoroki" storage="chara/hi/hi04.png"]
[chara_face name="hi" face="kanasimi" storage="chara/hi/hi05.png"]
[chara_face name="hi" face="ikari" storage="chara/hi/hi06.png"]

;-------------------------------ここから開始-----------------------------------

[playbgm storage="normal.ogg" loop="smooth"]

;背景→中庭
[camera x=230 y=-100 zoom=1.7 time=0] 
[image layer="0" folder="bgimage" name="nakaniwa_yu" storage="gaikan_yu.jpg" visible="true" top="0" left="0" width=1200 height=675]
[image layer="1" folder="bgimage" name="nakaniwa_yu" storage="gaikan_yu.jpg" visible=true top="0" left="0" width=1200 height=675]
[mask_off time=0]

@showmenubutton

[chara_show name="hi" left=70 top=60 layer="1" time=0]
[chara_show name="akane3" width="200" height="400" left=700 top=240 layer="1" effect="easeInSine" time=0 opacity=0]
[anim name="akane3" time="0" opacity=0]
[anim name="akane3" left="-=50" time=800 opacity=255]
[wa]

[chara_mod name="akane3" face="yorokobi"]

[playse storage="jump1.ogg" loop="false"]
[anim name="akane3" top="-=10" effect="easeInOutSine" time=100]
[wa]
[anim name="akane3" top="+=10" effect="easeInOutSine" time=100]
[wa]

[camera x=0 y=0 zoom=1 time=800]


@layopt layer=message0 visible=true

[fadeoutbgm time=500]

#あかね
……………………[p]

@layopt layer=message0 visible=false

[anim name="akane3" left="+=50" time=600 opacity=0]
[chara_hide_all layer="1"]

[playbgm storage="dozikkomarch.ogg" loop="smooth"]
[chara_mod name="akane" face="yorokobi" layer="1" time=0 opacity=0]
[chara_show name="akane" left=450 top=50 layer="1" time=0 opacity=0]
[anim name="akane" time="0" opacity=0]
[chara_mod name="hi" face="mumu" layer="1"]
[chara_show name="hi" left=170 top=60 layer="1"]
[anim name="akane" left="-=50" time=500 effect="easeInOutSine" opacity=255]
[wa]

@layopt layer=message0 visible=true

[anim name="akane" left="-=10" time=100]
[anim name="akane" left="+=10" time=100]
[anim name="akane" left="-=10" time=100]
[anim name="akane" left="+=10" time=100]

#あかね
[font size=15]……つんつん[p]

[chara_mod name="hi" face="odoroki" layer="1"]
[anim name="hi" top="-=20" time=100]
[playse storage="jump2.ogg" loop="false"]
[wa]
[anim name="hi" left="+=10" time=100]
[anim name="hi" left="-=10" time=100]
[anim name="hi" left="+=10" time=100]
[anim name="hi" left="-=10" time=100]
[anim name="hi" top="+=20" time=100]

#ひぃ
[font size=50]ぎゃ～！！[p]
[resetfont]

[chara_hide_all time=200 layer="1"]

[chara_mod name="hi" face="ikari" layer="1"]
[chara_show name="hi" left=80 top=60 layer="1" wait=false]
[chara_show name="akane" left=480 top=50 layer="1" wait=true]

#ひぃ
何するのよ！！！[r]
足しびれてたのに～！！！！[p]

[chara_mod name="akane" face="yorokobi" layer="1"]

#あかね
あはは、ごめんごめん[p]

[fadeoutbgm time=500]
[freeimage layer=1 time=400]

[image layer="1" folder="bgimage" name="kuro" storage="kuro.jpg" visible="true" top="650" left="0" time="100" wait=false]
[image layer="1" folder="bgimage" name="kuro" storage="kuro.jpg" visible="true" top="-820" left="0" time="100" wait=true]
[camera y=-250 time=500]

[playbgm storage="sample.ogg" loop="smooth"]

[chara_show name="akane2" time=300 left=480 top=60 layer="0" wait=false]
[chara_show name="hi2" time=300 left=80 top=50 layer="0" wait=true]

[anim name="akane2" left="-=50" time=700]

#あかね
ねえねえ、これは何？[p]

[anim name="hi2" left="+=50" time=700]

#ひぃ
ああ、それね。焼き芋よ[p]

[anim name="akane2" top="-=10" effect="easeInOutSine" time=100]
[wa]
[anim name="akane2" top="+=10" effect="easeInOutSine" time=100]
[wa]

#あかね
へ～、おいしそうだね！[p]

#ひぃ
今、焼くから待ってなさい[p]

@layopt layer=message0 visible=false

[fadeoutbgm time=500]
[mask]
[freeimage layer=0 time=0]
[freeimage layer=1 time=0]
[reset_camera]
[image layer="0" folder="bgimage" name="nakaniwa_yu" storage="gaikan_yu.jpg" visible="true" top="0" left="0" width=1200 height=675 time=0]
[chara_show name="akane" left=280 top=50 layer="0" time=0]
[wait time=500]
[mask_off]

[playbgm storage="sample.ogg" loop="smooth"]

@layopt layer=message0 visible=true

[mtext text="そわそわ" x=230 y=120 size=30 in_effect="fadeIn" in_sync=true time=0 out_effect="fadeOut" out_sync=true wait=false]
[anim name="akane" left="+=10" time=100]
[anim name="akane" left="-=10" time=100]
[anim name="akane" left="+=10" time=100]
[anim name="akane" left="-=10" time=100]
[wait time=1000]
[mtext text="そわそわ" x=620 y=270 size=30 in_effect="fadeIn" in_sync=true time=0 out_effect="fadeOut" out_sync=true wait=false]
[anim name="akane" left="+=10" time=100]
[anim name="akane" left="-=10" time=100]
[anim name="akane" left="+=10" time=100]
[anim name="akane" left="-=10" time=100]

#あかね
……[p]

@layopt layer=message0 visible=false

[anim name="akane" top="+=20" time=500]
[wa]
[chara_mod name="akane" face="kanasimi" layer="0"]
[wait time=1000]

[chara_hide_all time=400 layer="0"]

[chara_mod name="akane" face="kanasimi" layer="0" time=0]
[chara_show name="hi" left=50 top=60 layer="0" time=0 opacity=0]
[anim name="hi" time="0" opacity=0]
[chara_show name="akane" left=480 top=80 layer="0" wait=false]
[anim name="hi" time=100 left="+=50" effect="easeInOutSine" time=500 opacity=255 wait=true]

@layopt layer=message0 visible=true

#ひぃ
落ち着かないみたいだけど、どうしたの？[p]

#あかね
そろそろ焼けたかなって……[p]

[anim name="hi" top="+=30" time=500]
[wa]
[chara_mod name="hi" face="kanasimi" layer="0"]
[wait time=500]
[anim name="hi" top="-=30" time=500]

#ひぃ
そうね、もう少し時間が掛かると思うわ[p]

#あかね
分かった……[p]

@layopt layer=message0 visible=false

[fadeoutbgm time=500]

[mask]
[chara_hide_all layer="0" time=0]
[wait time=1000]
[mask_off]

[playbgm storage="sample.ogg" loop="smooth"]

[chara_mod name="hi" face="yorokobi" layer="0" time=0]
[chara_show name="hi" left=80 top=60 layer="0" opacity=0 wait=false]
[chara_show name="akane" left=480 top=50 layer="0" wait=true]

@layopt layer=message0 visible=true

#ひぃ
うん、いい感じ！[r]
はい、どうぞ[p]

[chara_mod name="akane" face="yorokobi" layer="0"]

[anim name="hi" left="+=30" time=300]
[wa]
[playse storage="jump1.ogg" loop="false"]
[anim name="akane" top="-=10" effect="easeInOutSine" time=100]
[anim name="akane" top="+=10" effect="easeInOutSine" time=100]
[wa]
[anim name="hi" left="-=20" time=300]
[wa]

#ひぃ
それじゃあ[p]

[playse storage="jump1.ogg" loop="false"]
[anim name="akane" top="-=10" effect="easeInOutSine" time=100]
[anim name="hi" top="-=10" effect="easeInOutSine" time=100]
[anim name="akane" top="+=10" effect="easeInOutSine" time=100]
[anim name="hi" top="+=10" effect="easeInOutSine" time=100]
[fadeoutbgm time=500]

#あかね・ひぃ
[font size=50]「「いっただきまーす！」」[p]
[resetfont]

[chara_hide_all time=300]

[chara_mod name="hi" face="odoroki" time=0 opacity=0]
[chara_show name="hi" left=30 top=80 layer="0" time=0 opacity=0]
[anim name="hi" time="0" opacity=0]
[chara_mod name="akane" face="odoroki" layer="0" time=0]
[chara_show name="akane" left=280 top=80 layer="0" time=300]

#あかね
……う、[l]
[camera y=60 zoom=1.7 time=500]
[playbgm storage="comedy.ogg" loop="smooth"]
[playse storage="jump1.ogg" loop="false"]
[anim name="akane" left="+=10" time=100]
[anim name="akane" left="-=10" time=100]
[anim name="akane" left="+=10" time=100]
[anim name="akane" left="-=10" time=100]
[font size=50]うぅ～！！！！！[p]
[resetfont]

[anim name="hi" left="+=50" effect="easeInOutSine" time=500 opacity=255]


#ひぃ
……熱いって！？[r]
大変、お水お水！[p]


@layopt layer=message0 visible=false

;BGM停止
[fadeoutbgm time=3000]
;暗転
[mask time=3000]
[reset_camera time=0]
[chara_hide_all]
[freeimage layer="0"]
[mask_off]
[jump first.ks]